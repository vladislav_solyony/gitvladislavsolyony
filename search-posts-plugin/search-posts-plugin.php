<?php 
/*
Plugin Name: Search Posts 
Plugin URI:  -
Description: Plugin to search & filter posts on website.
Version:     1.0
Author:      Vladislav Solyony
Author URI:  -
Text Domain: wporg
Domain Path: -
License:     GPL2
 
Search Posts Plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
Search Posts Plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {License URI}.
*/


require_once (plugin_dir_path(__FILE__) . 'search-posts-widget.php' );

add_action( 'wp_enqueue_scripts', 'register_widget_search_posts_styles' );

function register_widget_search_posts_styles() {
        wp_register_style( 'search-posts-css', plugins_url( 'search-posts.css', __FILE__ ) );
        wp_enqueue_style( 'search-posts-css' );  
}

                                              

function search_posts_install()
{
    // trigger our function that registers the custom post type
    events_register_post_type();
 
    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'search_posts_install' ); //Activation Hook

//DEACTIVATION
function search_posts_deactivation()
{
    // our post type will be automatically removed, so no need to unregister it
 
    // clear the permalinks to remove our post type's rules
    flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'search_posts_deactivation' ); //Deactivation Hook

