(function($){
    
    var postsSearch = $("#ps-posts-search");
    
    var searchFrom = postsSearch.find("#ps-search-form");
    
    $(function() {
        $("#ps-search-form").on('submit', function(e){
            
            var data = $(this).serialize();
            
            $.ajax({
                url:  myajax.url,
                data: data,
                type: 'GET',
                dataType:'json',
                success: function(response) {
                     if( response){   $(".posts-list").html("")     
                    for(var i = 0 ; i < response.length ; i++) { 
                        var html2 = "<li id='post-" + 
                                    response[i].id + "'><a href='" + response[i].link + "'>" + 
                                    response[i].title + "</a></li><hr>";       
                        $('.posts-list').append(html2);  
                    }             }
                }
                   
            });
            
            return false;
            
        });
        
        
    });
})(jQuery);