<?php
<<<<<<< HEAD
=======
Error_Reporting(E_ALL & ~E_NOTICE);
>>>>>>> 3b8ede6bc4ea90cd5fa1dfbf20c964386b160de6

add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
function myajax_data(){

    wp_localize_script( 'posts-search-js', 'myajax', 
        array(
            'url' => admin_url( 'admin-ajax.php' )
        )
    );  

}


add_action( 'wp_enqueue_scripts', 'posts_search_scripts' );
function posts_search_scripts() {
    wp_register_script( 'posts-search-js', plugins_url( '/posts-search.js', __FILE__ ) );
    wp_enqueue_script( 'posts-search-js' );
}
 
class Search_posts_widget extends WP_Widget
{  
    public function __construct() {
        parent::__construct( "search_posts_widget", "Search posts widget",
            array( "description" => "A simple widget to search & filter posts on website" ) );
    }
    
    public function form( $instance ) {
        $number = "";
    
        if ( ! empty( $instance ) ) {
            $number = $instance["number"];
        }
 
        $numberId = $this->get_field_id( "number" );
        $numberName = $this->get_field_name( "number" );
        echo '<label for="' . $numberId . '">Number of posts in AJAX filter</label><br>';
        echo '<input id="' . $numberId . '" type="number" name="' .
        $numberName . '" value="' . $number . '"><br>';
        
        return $number;
    }
    
    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["number"] = htmlentities( $newInstance["number"] );
        return $values;
    }
    
    public function widget( $args, $instance ) { 
    ?>
    
        <div id="ps-posts-search">
            <h3>AJAX Posts filter</h3>
            <form id='ps-search-form' action="" method="get">
                <label for="ps-post-name">Post name</label>
                    <input type="text" id="ps-post-name" name="ps-post-name" value="">
                <label for="ps-post-date">Post date</label>
                    <input type="date" id="ps-post-date" name="ps-post-date" value="">
                <button type="submit">search</button>
                <input type=hidden name=action value=posts_search>
                <input type="hidden" name="ps-post-number" value="<?php echo $instance['number']; ?>">
            </form>
        
            <ul class="posts-list"></ul>
        </div>
    <?php 
    } 
}

add_action( 'wp_ajax_posts_search', 'posts_search_callback' );
add_action( 'wp_ajax_nopriv_posts_search', 'posts_search_callback');
 
function posts_search_callback() {    
    $p_name =  $_GET['ps-post-name'] ;
    $p_date = date( 'Y-m-d', strtotime( $_GET['ps-post-date'] ) );
    $numb = $_REQUEST['ps-post-number'];
    
    $querystr = "
        SELECT ID, post_title, post_date FROM wp_posts 
        WHERE post_title LIKE '%{$p_name}%'
        AND post_date > $p_date
        AND post_status='publish'
        LIMIT $numb
    ";

    $wpdb_results = $GLOBALS['wpdb']->get_results( $querystr ); 
    
        if( $wpdb_results ) {
            foreach ( $wpdb_results as $res ) {
                $result[] = array(
                    'id'        => $res->ID,
                    'title'     => $res->post_title,
                    'date'      => $res->post_date,
                    'link'      => get_permalink($res->ID) 
                );    
            }
        } else {
            echo 'not found';
        }
    
    wp_die( json_encode( $result ) );
}
     
add_action( "widgets_init", function () {
    register_widget( "Search_posts_widget" );
});      


