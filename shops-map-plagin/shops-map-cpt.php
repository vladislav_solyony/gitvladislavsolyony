<?php
//ACTIVATION
function shops_register_post_type(){
    $labels = array(
        'name'          => 'Shops',
        'singular_name' => 'Shop', 
        'add_new'       => 'Add shop',
        'add_new_item'  => 'Add new shop', 
        'edit_item'     => 'Edit shop',
        'new_item'      => 'New shop',
        'all_items'     => 'All shops',
        'view_item'     => 'Browse shops',
        'search_items'  => 'Search shops',
        'not_found'     => 'Shops not found.',
        'menu_name'     => 'Shops'
    );
    $args = array(
        'labels'          => $labels,
        'public'          => true,
        'menu_icon'       => 'dashicons-cart', 
        'menu_position'   => 5,
        'has_archive'     => true,
        'query_var'       => true,

        'rewrite'         => array(
            'slug'        => 'shops',
            'with_front'  => true,
            'pages'       => true,
            'feeds'       => false,
        )
    );
    register_post_type( 'shop', $args );
}

add_action( 'init', 'shops_register_post_type' );
