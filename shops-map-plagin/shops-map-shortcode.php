<?php

add_action( 'wp_enqueue_scripts', 'shop_map' );  
function shop_map() {      
    wp_register_script( 'my_map_api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAZ7cl8R-wFJs5_VwiXq-Egpd11HaUbDT0' );
    wp_register_script( 'shops_map_js', plugins_url( '/shops-map.js', __FILE__ ), array( 'jquery', 'my_map_api' ) );
    wp_enqueue_script( 'my_map_api' );       
}  

function shops_info( $atts ) {
    extract( shortcode_atts (array(
        'ids'    => array(152), 
        'limit'  => 10,
        'width'  => 500,
        'height' => 300,
    ), $atts) );
    
    $integerIDs = array_map( 'intval', explode( ',', $atts['ids'] ) );    
                                     
    $args = array(
        'post_type'      => 'shop',
        'posts_per_page' => $atts['limit'],
        'post__in'       => $integerIDs
    );
        
    $shop_query = new WP_Query( $args );
    while( $shop_query->have_posts() ) {
        $shop_query->the_post();
        
        //Get Shop coordinates    
        $address  = str_replace( " ", "+", get_post_meta( get_the_ID(), 'shop_address', true ) ); // replace all the white space with "+" sign to match with google search pattern 
        $url      = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address"; 
        $response = file_get_contents( $url ); 
        $json     = json_decode( $response, TRUE ); //generate array object from the response from the web

        $result[] = array(
            'id'               => get_the_ID(),
            'title'            => get_the_title(),
            'permalink'        => get_permalink(),
            'shop_name'        => get_post_meta( get_the_ID(), 'shop_name', true ),
            'shop_description' => get_post_meta( get_the_ID(), 'shop_description', true ),
            'shop_address'     => get_post_meta( get_the_ID(), 'shop_address', true ),
            'lat'              => $json['results'][0]['geometry']['location']['lat'],
            'lgn'              => $json['results'][0]['geometry']['location']['lng'],
        );
    }
    
    wp_enqueue_script( 'shops_map_js' );                           
    wp_localize_script( 'shops_map_js', 'shops_array', $result );
     
    ?>
    
    <div id="output-map-block" style="width: <?php echo $atts['width']; ?>px ; height: <?php echo $atts['height']; ?>px;"> </div>
    
    <?php   
}

add_shortcode( 'shops_info', 'shops_info' );
?>
