(function($){

//Add Map on page
var map;
map = new google.maps.Map(document.getElementById('output-map-block'), {
    //center
    //zoom
});   

//Add markers & infoboxes of shops
var geocoder = new google.maps.Geocoder();

bounds  = new google.maps.LatLngBounds();
var infowindows = new Array(shops_array.length);
    
for( var i in shops_array ) {

    var contentString = "<h3>" + shops_array[i].shop_name + "</h3><br><p>" + shops_array[i].shop_address + "</p>";
    infowindows[shops_array[i].shop_name] = new google.maps.InfoWindow({content: contentString});
    var marker = new google.maps.Marker({
        position: {lat: shops_array[i].lat, lng: shops_array[i].lgn},
        map: map,
        title: shops_array[i].shop_name
    });

    loc = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
    bounds.extend(loc);
    marker.addListener('click', function() {
        infowindows[this.title].open(map, this);
    });
}

map.fitBounds(bounds);       // auto-zoom
map.panToBounds(bounds);     // auto-center
    
})(jQuery);
