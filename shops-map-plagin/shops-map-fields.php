<?php 

function shops_add_custom_metabox() {   
    add_meta_box( 'shop-meta', 
                  'Shop information', 
                  'shops_meta_callback', 
                  'shop'
    );
}

add_action( 'add_meta_boxes', 'shops_add_custom_metabox' );

function shops_meta_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'shops_shop_nonce' );

    $shop_name        = get_post_meta( $post->ID, 'shop_name', true );
    $shop_description = get_post_meta( $post->ID, 'shop_description', true );
    $shop_address     = get_post_meta( $post->ID, 'shop_address', true );
    
    ?>
    
    <div>   
        <div class="row">   
            <div class="th">
                <label for="shop_name">Shop Name</label>
            </div>
            <div class="td">
                <input style="width: 631px" type="text" name="shop_name" id="shop_name" value="<?php echo $shop_name; ?>">
            </div>
        </div>
    </div>
    <div>
        <div class="row">  
            <div class="th">
                <label for="shop_description">Shop Description</label>
            </div>
            <div class="td">
                <textarea id="shop_description" name="shop_description" cols="100" rows="5"><?php echo $shop_description; ?></textarea>
            </div>
        </div>
    </div>
    <div>
        <div class="row">  
            <div class="th">
                <label for="shop_address">Shop Address</label>
            </div>
            <div class="td">
                <input style="width: 631px" type="text" id="shop_address" name="shop_address" value="<?php echo $shop_address; ?>">
            </div>
        </div>
    </div>
    
    <?php   
}
 
function shops_meta_save( $post_id ){
    //Cheks save status
    $is_autosave    = wp_is_post_autosave( $post_id );
    $is_revision    = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST['shops_shop_nonce'] ) && wp_verify_nonce( $_POST['shops_shop_nonce'], basename( __FILE__ ) ) ) ? true : false;
     
    //Exit script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
     
    if ( isset( $_POST['shop_name'] ) ) {
        update_post_meta( $post_id, 'shop_name', sanitize_text_field( $_POST['shop_name'] ) );
    }
     
    if ( isset( $_POST['shop_description'] ) ) {
        update_post_meta( $post_id, 'shop_description', sanitize_text_field( $_POST['shop_description'] ) );
    }
    
    if ( isset( $_POST['shop_address'] ) ) {
        update_post_meta( $post_id, 'shop_address', sanitize_text_field( $_POST['shop_address'] ) );
    }
     
    if ( isset( $_POST['shop_lat'] ) ) {
        update_post_meta( $post_id, 'shop_lat', sanitize_text_field( $_POST['shop_lat'] ) );
    }
     
    if ( isset( $_POST['shop_lng'] ) ) {
        update_post_meta( $post_id, 'shop_lng', sanitize_text_field( $_POST['shop_lng'] ) );
    }
}
add_action( 'save_post', 'shops_meta_save' );
