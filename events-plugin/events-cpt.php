<?php
//ACTIVATION
function events_register_post_type(){
    $labels = array(
        'name'          => 'Events',
        'singular_name' => 'Events', 
        'add_new'       => 'Add event',
        'add_new_item'  => 'Add new event', 
        'edit_item'     => 'Edit event',
        'new_item'      => 'New event',
        'all_items'     => 'All events',
        'view_item'     => 'Browse events',
        'search_items'  => 'Search events',
        'not_found'     => 'Events not found.',
        'menu_name'     => 'Events'
    );
    $args = array(
        'labels'          => $labels,
        'public'          => true,
        'menu_icon'       => 'dashicons-star-filled', 
        'menu_position'   => 5,
        'has_archive'     => true,
        'query_var'       => true,

        // 'capabilities  => array(),
        'rewrite'         => array(
            'slug'        => 'events',
            'with_front'  => true,
            'pages'       => true,
            'feeds'       => false,
        )
    );
    
    register_post_type( 'event', $args );
}

add_action( 'init', 'events_register_post_type' );

//TAXONOMY 
function events_register_taxonomy(){
    $labels = array(
        'name'                       => 'Cities',
        'singular_name'              => 'City',
        'search_items'               => 'Search cities',
        'popular_items'              => 'Popular cities',
        'all_items'                  => 'All cities',              
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => 'Edit city',
        'update_item'                => 'Update city',
        'add_new_item'               => 'Add new city',
        'new_item_name'              => 'New city',
        'separate_items_with_commas' => 'Separate cities',
        'add_or_remove_items'        => 'Add or remove city',
        'choose_from_most_used'      => 'Choose from most used cities',
        'not_found'                  => 'Cities not found',
        'menu_name'                  => 'Cities',   
    );
    
    $args = array(
        'hierarchical'          => true,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'cities' ),
    );
    
    register_taxonomy( 'cities', 'event', $args );
    
}

add_action( 'init', 'events_register_taxonomy');