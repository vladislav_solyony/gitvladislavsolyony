<?php 
/*
Plugin Name: Events Plugin
Plugin URI:  -
Description: Plugin to track upcoming events dates and status
Version:     1.0
Author:      Vladislav Solyony
Author URI:  -
Text Domain: wporg
Domain Path: -
License:     GPL2
 
Events Plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
Events Plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {License URI}.
*/

/*//Exit if accesed directly
if( ! define( 'ABSPATH') ){
    exit;
} */

require_once (plugin_dir_path(__FILE__) . 'events-cpt.php' );
require_once (plugin_dir_path(__FILE__) . 'events-fields.php' );
require_once (plugin_dir_path(__FILE__) . 'events-widget.php' );
require_once (plugin_dir_path(__FILE__) . 'events-shortcode.php' );

add_action( 'wp_enqueue_scripts', 'register_widget_events_styles' );

function register_widget_events_styles() {
        wp_register_style( 'events-widget-css', plugins_url( 'css/widget-events.css', __FILE__ ) );
        wp_enqueue_style( 'events-widget-css' );  
}

function events_install()
{
    // trigger our function that registers the custom post type
    events_register_post_type();
 
    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}

register_activation_hook( __FILE__, 'events_install' ); //Activation Hook

//DEACTIVATION
function events_deactivation()
{
    // our post type will be automatically removed, so no need to unregister it
 
    // clear the permalinks to remove our post type's rules
    flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'events_deactivation' ); //Deactivation Hook

//UNINSTALL
//register_uninstall_hook(__FILE__, 'events_function_to_run');// Uninstall Hook

