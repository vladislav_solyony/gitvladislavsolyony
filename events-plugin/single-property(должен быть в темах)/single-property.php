<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

global $post;
 
get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php 
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/post/content', get_post_format() );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

					the_post_navigation( array(
						'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'twentyseventeen' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
						'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'twentyseventeen' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',
					) );

				endwhile; // End of the loop. 
                ?>
                
            <h2>
                <?php if ( $name = types_render_field( 'name' ) ) : ?>
                    <?php echo $name; ?>
                <?php endif; ?>
            </h2>
                <table>
                    <tbody>
                        <tr>
                            <td>Square(m<sup>2</sup>):</td>
                            <td>
                                <?php if ( $square = types_render_field( 'square' ) ) : ?>
                                    <?php echo $square; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>   
                            <td>Price($):</td>
                            <td>
                                <?php if ( $price = types_render_field( 'price' ) ) : ?>
                                    <?php echo $price; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>    
                            <td width="100%">
                                <?php if ( $photo = types_render_field( 'photo' ) ) : ?>
                                    <?php echo $photo; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: justify">
                                <?php if ( $description = types_render_field( 'description' ) ) : ?>
                                    <?php echo $description; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            
            <h3>Additional services and information</h3>
            
            <table>
                <tbody>                         
                    <tr>                        
                        <td>Location:</td>
                        <td>
                            <?php if ( $address = get_field( 'acf_address', $post->ID ) ) : ?>
                                <?php echo $address; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Furniture:</td>
                        <td>
                            <?php if ( $furniture = get_field( 'acf_furniture', $post->ID ) ) : ?>
                                <?php echo $furniture; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Additional services:</td>
                        <td>
                            <?php 
                                $values = get_field('acf_additional_services');
                                if(isset($values))
                                {
                                    echo '<ul>';
                                    foreach($values as $value)
                                    {
                                         echo '<li>' . $value . '</li>';
                                    }
                                    echo '</ual>';
                                }
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
