<?php 
function events_add_custom_metabox(){
    
add_meta_box( 'event-meta', 
              'Event Listing', 
              'events_meta_callback', 
              'event'
            );
}

add_action( 'add_meta_boxes', 'events_add_custom_metabox');

function events_meta_callback( $post ){
    wp_nonce_field( basename( __FILE__ ), 'events_event_nonce' );
    $events_stored_status = get_post_meta( $post->ID, 'event_status', true );
    $events_stored_date = get_post_meta( $post->ID, 'event_date', true );
    ?>
    
    <div>   
        <div class="row">   
            <div class="th">
                <label for="event-status">Event status</label>
            </div>
            <div class="td">
                <input type="radio" name="event_status"  id="event_status" value="Open"  <?php checked('Open', $events_stored_status); ?>>Open<br>
                <input type="radio" name="event_status" id="event_status2" value="Closed"<?php checked('Closed', $events_stored_status); ?>>Closed<br>
            </div>
        </div>
    </div>
    <div>
        <div class="meta">  
            <div class="th">
                <label for="event-date">Event date</label>
            </div>
            <div class="td">
                <input type="date" id="event_date" name="event_date" value="<?php if ( ! empty ( $events_stored_date ) ) echo esc_attr( $events_stored_date ); ?>">
            </div>
        </div>
    </div>
    
    <div class="meta-editor">
    </div>
    <?php   
 }
 
 function events_meta_save( $post_id){
     //Cheks save status
     $is_autosave = wp_is_post_autosave( $post_id );
     $is_revision = wp_is_post_revision( $post_id );
     $is_valid_nonce = ( isset( $_POST[ 'events_event_nonce' ] ) && wp_verify_nonce( $_POST[ 'events_event_nonce'], basename( __FILE__ ) ) ) ? 'true' : 'false';
     
     //Exit script depending on save status
     if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
         return;
     }
     
     if ( isset( $_POST['event_status'] ) ){
         update_post_meta( $post_id, 'event_status', sanitize_text_field( $_POST[ 'event_status'] ) );
     }
     
     if ( isset( $_POST['event_date'] ) ){
         update_post_meta( $post_id, 'event_date', sanitize_text_field( $_POST[ 'event_date'] ) );
     }
 }
 add_action( 'save_post', 'events_meta_save' );