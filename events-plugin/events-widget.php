<?php

class Events_widget extends WP_Widget
{
    public function __construct() {
        parent::__construct( "events_widget", "Events widget",
            array( "description" => "A simple widget to display upcoming events" ) );
    }
    
    public function form( $instance ) {
        $number = "";
        $status = "";
    
        if ( ! empty( $instance ) ) {
            $number = $instance["number"];
            $status = $instance["status"];
        }
 
        $numberId = $this->get_field_id( "number" );
        $numberName = $this->get_field_name( "number" );
        echo '<label for="' . $numberId . '">Number</label><br>';
        echo '<input id="' . $numberId . '" type="number" name="' .
        $numberName . '" value="' . $number . '"><br>';
     
        $statusId = $this->get_field_id( "status" );
        $statusName = $this->get_field_name( "status" );
    
        echo '<select name="' . $statusName . '" id="' . $statusId . '">
                <option>Open</option>
                <option>Closed</option>
              </select>';
    }
    
    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["number"] = htmlentities( $newInstance["number"] );
        $values["status"] = htmlentities( $newInstance["status"] );
        return $values;
    }
    
    public function widget( $args, $instance ) { 
        global $post;  
        $temp_post = $post;
            
        $args = array(
                'numberposts' =>  $instance['number'],
                'category' => 0, 'orderby' => 'date',
                'order' => 'DESC', 'include' => array(),
                'exclude' => array(), 'meta_key' => 'event_status',
                'meta_value' =>$instance['status'], 'post_type' => 'event',
                'suppress_filters' => true
        );
        
        $get_post = get_posts( $args ); ?>
        
        <div class="event-block">
            <h2>Upcoming events:</h2>
            <?php if ( $get_post ): ?>
            <?php foreach ( $get_post as $post ) : setup_postdata( $post ); ?>
                <h3><?php the_title(); ?></h3>
                <p class="event-date"><?php echo get_post_meta(get_the_ID(), 'event_date', true); ?></p>
                <hr>
            <?php endforeach; 
            wp_reset_postdata();
            setup_postdata( $temp_post ); ?>
            <?php else : ?>
                <p> No posts found. </p>
            <?php endif; ?>
        </div>             
        
        <?php
        $title  = $get_post;
        $number = $instance["number"];
        $status = $instance["status"];  
    }
}

add_action( "widgets_init", function () {
    register_widget( "Events_widget" );
});

