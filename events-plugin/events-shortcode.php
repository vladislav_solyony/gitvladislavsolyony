<?php 
function events_list( $atts ) {
    extract(shortcode_atts(array(
        "number" => 10,
        "status" => 'Open'
    ), $atts));
    
    $args = array(
                'numberposts' => $atts['number'],
                'category' => 0, 
                'orderby' => 'date',
                'order' => 'DESC', 
                'include' => array(),
                'exclude' => array(),
                'meta_key' => 'event_status',
                'meta_value' =>$atts['status'], 'post_type' => 'event',
                'suppress_filters' => true
    );
        
    $get_post = get_posts( $args ); ?>
    
    <?php 
        global $post; 
        $temp_post = $post; 
    ?>
        
    <div class="event-block">
        <h2>List of events:</h2>
        <?php if ( $get_post ): ?>
        <?php foreach ( $get_post as $post ) : setup_postdata( $post ); ?>
            <h3><?php the_title(); ?></h3>
            <p class="event-date"><?php echo get_post_meta(get_the_ID(), 'event_date', true); ?></p>
            <hr>
        <?php endforeach; 
        wp_reset_postdata();
        setup_postdata( $temp_post ); ?>
        <?php else : ?>
            <p> No posts found. </p>
        <?php endif; ?>
    </div> 
    <?php
}

add_shortcode( 'events_list', 'events_list');