            <?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/header.php" ?>  
            
            <!-- HERO -->
            <div class="container fullwidth pt140 pb140 overlay-dark" data-background="/borano-template/img/gallery/2.jpg">
                <div class="pt140 pb140">
                    <div class="entrance">
                        <h2 class="title">Get In Touch</h2>
                    </div>
                </div>
            </div>
            <!-- /HERO -->

            <div class="container pt100 pb100">
                <div class="row">
                    
                    <!-- CONTACT DETAILS -->
                    <div class="col-md-4 col-md-offset-1">

                        <h3 class="title title-tooltip">Contact Us</h3>

                        <p class="small mb40"><?php echo $contactInfo["contact_text"]; ?></p>
                        
                        <div class="contact-info">
                            <a href="#">
                                <b>Mail:</b> <?php echo $contactInfo["contact_mail"]; ?>
                            </a>
                            <a href="#">
                                <b>Address:</b> <?php echo $contactInfo["contact_address"]; ?> 
                            </a>
                            <a href="#">
                                <b>Phone:</b> <?php echo $contactInfo["contact_phone"]; ?>
                            </a>
                        </div>
                        
                    </div>
                    <!-- /CONTACT DETAILS -->

                    <!-- CONTACT FORM -->
                    <div class="col-md-6">

                        <div class="contact-message"></div>
                    
                        <form id="contact" action="/send.php">
                            <input name="name" id="name" type="text" placeholder="Name:" required autocomplete="off">
                            <input name="email" id="email" type="email" placeholder="Email:" required autocomplete="off">
                            <textarea name="message" id="message" placeholder="Message:" required rows="4"></textarea>
                            <input type="submit" id="submit" value="send" class="button outline">
                        </form>

                    </div>
                    <!-- /CONTACT FORM -->

                </div>
            </div>

            <!-- GOOGLE MAP -->
            <!--<div id="google-map"></div>-->   
            
            <?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/footer.php" ?>