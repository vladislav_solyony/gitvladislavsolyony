<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/header.php" ?>
      
<!-- BLOG POSTS -->
            <?php foreach($blogList as $blog_note): ?>
            <div class="container pt140 pb140" data-background="<?php echo $blog_note["title_img"]; ?>">
                <div class="pt140 pb140">

                    <a href='/blog/<?php echo $blog_note["code"]; ?>' class="entrance">
                        <h3 class="title mb10"><?php echo htmlspecialchars($blog_note["title"], ENT_QUOTES, 'UTF-8'); ?></h3>
                        <p class="separator"></p>
                        <p class="small capitalize serif"><?php echo htmlspecialchars($blog_note["date"], ENT_QUOTES, 'UTF-8'); ?></p>
                    </a>
                    
                </div>
            </div>
            <?php endforeach; ?>
             
<!-- /BLOG POSTS -->

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/footer.php" ?>