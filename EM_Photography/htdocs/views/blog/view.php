<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/header.php" ?>

<!-- POST CONTENT -->


            <div class="container mt70 mb70 text-center equal-height-cols">
                <div class="row">
                
                    <div class="col-md-8 col-md-offset-2" style="margin-top: 100px;">
                        <h3 class="title mb0"><?php echo $blogArticle['bl_title']; ?></h3>
                        <p class="separator mt20 mb20"></p>
                        <p><?php echo $blogArticle['bl_text']; ?></p>
                        
                        <!-- YOUTUBE VIDEO -->
                        <!--<div class="video-container mt70">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/I5sJhSNUkwQ" frameborder="0" allowfullscreen></iframe>
                        </div>-->

                        <!-- BLOCKQUOTE -->
                        <blockquote class="mt90">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae quo, suscipit accusamus laboriosam fugiat, perferendis dolorem numquam fuga ea vitae.</p>
                            <footer>Vinme Mesxi Meleqse</footer>
                        </blockquote>
                    </div>
                
                </div>
            </div>
  
  
<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/footer.php" ?>