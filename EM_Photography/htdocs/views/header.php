<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Evgeniy Menyaylov Photography</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/borano-template/img/favicon.png">
        <!-- plugins' css -->
        <link rel="stylesheet" href="/borano-template/css/plugins.css">
        <!-- google fonts -->
        <link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300%7cPoppins:400,300,500,700,600' rel='stylesheet' type='text/css'>
        <!-- main css file -->
        <link rel="stylesheet" href="/borano-template/css/main.css">
        <!-- modernizr -->
        <script src="/borano-template/js/lib/modernizr-2.6.2.min.js"></script>
    </head>

    <body>
        <!-- MAIN_WRAPPER -->
        <div class="main-wrapper animsition">

            <!-- HEADER -->
            <header id="header" class="fixed">
                <!-- LOGO -->
                <a href="/main" class="logo">
                    <!--<img src="/borano-template/img/logo.png" class="logo-dark" alt="">
                    <img src="/borano-template/img/logo-light.png" class="logo-light" alt="">-->
                    <p class="logo">Evgeniy Menyaylov</p>
                </a>
                <!-- MOBILE MENU ICON -->
                <a href="#" class="mob-menu"><i class="fa fa-bars"></i></a>
                <!-- MENU -->
                <nav>
                    <ul class="main-menu">
                        <li>
                            <a href="/main">home</a>
                        </li>
                        <li>
                            <a href="/gallery">gallery</a>
                        </li>
                        <li>
                            <a href="/about">about</a>
                        </li>
                        <li>
                            <a href="/blog">blog</a>
                        </li>
                        <li>
                            <a href="/contact">contact</a>
                        </li>
                    </ul>
                </nav>
            </header>
            <!-- /HEADER -->