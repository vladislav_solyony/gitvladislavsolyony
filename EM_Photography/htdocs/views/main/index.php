<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/header.php" ?>

<!-- KENBURNS SLIDESHOW -->
    <div class="kenburns swiper-container" data-autoplay="true" data-interval="5500">
        <div class="swiper-wrapper">
        <?php foreach($sliderPhotos as $slider): ?>
            <div class="swiper-slide" data-caption="Like the legend of the Phoenix" data-link="">
                <div data-background="<?php echo $slider["value"]; ?>"></div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
<!-- /KENBURNS SLIDESHOW -->

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/footer.php" ?>