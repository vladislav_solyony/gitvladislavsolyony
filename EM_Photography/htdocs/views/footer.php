            <!-- FOOTER -->
            <footer id="footer">
                
                <!-- FOOTER LINKS -->
                <div class="footer-links">
                     2016. By <a href="https://vk.com/vladislav_solyony">Vladislav Solyony</a>. |
                    <a href="#">Facebook</a> |
                    <a href="http://instagram.com/evgenii.menyailov/  ">Instagram</a> | 
                    <a href="http://vk.com/id21236034">VK</a>
                </div>
                <div class="footer-links">
                &copy; Template by AchtungThemes
                </div>

            </footer>
            <!-- /FOOTER -->

        </div>    
        <!-- /MAIN-WRAPPER -->
        
        <!-- js -->
        <script src="/borano-template/js/lib/jquery.min.js"></script>
        <script src="/borano-template/js/lib/scripts.js"></script>
        <!-- main js -->
        <script src="/borano-template/js/main.js"></script>

    </body>
</html>