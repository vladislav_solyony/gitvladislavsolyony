<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/header.php" ?>           
           
            <!-- CONTENT -->
            <div class="container fullwidth no-padding">
                
                <!-- IMAGE BACKGROUND -->
                <div class="col-md-6 col-sm-5 fullscreen-height fixed hidden-xs" style="margin-top: 50px;" data-background="<?php echo $authorInfo['author_photo']; ?>"></div>

                <!-- DETAILS -->
                <div class="col-md-6 col-sm-7 col-md-offset-6 col-sm-offset-5 fullscreen-height">
                    <div class="vcenter-outer p70 pt130">
                        
                    
                        <div class="vcenter-inner">
                            <h5 class="title-tooltip">Hello.</h5>
                            <h3 class="title mt0">I'm <?php echo $authorInfo['author_name']; ?>, I Like To Capture The Perfect</h3>
                            <p class="separator-left mb20"></p>
                            <p><?php echo $authorInfo['author_text']; ?></p>
                            <a href="contact" class="button mt10">contact me</a>
                        </div>
                    

                    </div>
                </div>
                <!-- /DETAILS -->

            </div>
            <!-- /CONTENT -->
            
<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/footer.php" ?>