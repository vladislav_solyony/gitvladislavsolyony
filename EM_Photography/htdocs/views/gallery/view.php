<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/header.php" ?>

            <!-- GALLERY -->
            <div class="container" style="margin-top: 110px;">
                <div class="justified">
                <?php foreach($photosItem as $photo): ?>
                    <a href="<?php echo $photo["link"]; ?>" data-rel="lightcase:gallery" title="<?php echo $photo["title"]; ?>">
                        <img src="<?php echo $photo["photo"]; ?>" alt="">
                    </a>
                <?php endforeach; ?>    
                </div>
            </div>
            <!-- /GALLERY -->

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/footer.php" ?>

