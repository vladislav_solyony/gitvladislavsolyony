<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/header.php" ?>
            

            <!-- ALBUMS -->
            <!-- For item sizes use classes .wide and .tall -->
            <div class="container" style="margin-top: 110px;">
                <div class="grid albums" data-gutter="5" data-columns="3">
                <?php foreach($albumList as $album): ?>
                    <div class="grid-item">
                        <div data-background="<?php echo $album["main_photo"]; ?>">
                            <!-- lightbox expand link -->
                            <a href="<?php echo $album["main_photo"]; ?>" class="expand" data-rel="lightcase:gal" title="<?php echo $album["title"]; ?>">
                                <i class="fa fa-expand"></i>
                            </a>
                            <!-- title -->
                            <div class="title">
                                <a href="/gallery/<?php echo $album["id"]; ?>" class="link"><?php echo $album["name"]; ?></a>
                            </div>                            
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>            
            <!-- /ALBUMS -->

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/views/footer.php" ?>
 