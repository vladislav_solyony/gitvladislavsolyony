<?php
return array(

    'news/([0-9]+)' => 'news/view/$1',
    'news' => 'news/index', 

    'gallery/([0-9]+)' => 'gallery/view/$1',
    'gallery' => 'gallery/index',
    
    'about' => 'about/index',
    
    'blog/(\D)' => 'blog/view/$1',
    'blog' => 'blog/index',
    
    'contact' => 'contact/index',
    
    'send.php' => '/contact.php',
    
    'main' => 'main/index',
    '' => 'main/index',
);