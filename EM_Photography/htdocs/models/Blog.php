<?php


class Blog
{

    /** Returns single news items with specified id
    * @rapam integer &id
    */

    public static function getArticlesByID($code)
    {
        //$id = intval($id);

        if ($code) {
            $db = DB::getConnection();
            $result = $db->query("SELECT * FROM blog WHERE bl_code='{$code}'");

            $result->setFetchMode(PDO::FETCH_ASSOC);

            $blogArticle = $result->fetch();

            return $blogArticle;
        }

    }

    /**
    * Returns an array of news items
    */
    public static function getBlogList() 
    {
        $db = DB::getConnection();
        $blogList = array();

        $result = $db->query('SELECT * FROM blog WHERE bl_active = 1');

        $i = 0;
        while($row = $result->fetch()) {
            $blogList[$i]['id']         = $row['bl_id'];
            $blogList[$i]['date']       = $row['bl_date'];
            $blogList[$i]['title']      = $row['bl_title'];
            $blogList[$i]['title_img']  = $row['bl_title_img'];
            $blogList[$i]['short_text'] = $row['bl_short_text'];
            $blogList[$i]['text']       = $row['bl_text'];
            $blogList[$i]['code']       = $row['bl_code'];
            $i++;
        }

        return $blogList;
    
    }

}