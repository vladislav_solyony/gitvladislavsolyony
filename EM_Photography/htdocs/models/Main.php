<?php


class Main
{

    /**
    * Returns an array of news items
    */
    public static function getSliderPhotos() 
    {
        $db = DB::getConnection();
        $sliderPhotos = array();

        $result = $db->query("SELECT * FROM new_main WHERE ma_name = 'slider_photo'");

        $i = 0;
        while($row = $result->fetch()) {
            $sliderPhotos[$i]['id']    = $row['ma_id'];
            $sliderPhotos[$i]['name']  = $row['ma_name'];
            $sliderPhotos[$i]['value'] = $row['ma_value'];

            $i++;
        }

        return $sliderPhotos;
        
    }

}