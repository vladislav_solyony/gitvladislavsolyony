<?php


class News
{

    /** Returns single news items with specified id
    * @rapam integer &id
    */

    public static function getNewsItemByID($id)
    {
        $id = intval($id);

        if ($id) {
            $db = DB::getConnection();
            $result = $db->query('SELECT * FROM blog WHERE id=' . $id);

            $result->setFetchMode(PDO::FETCH_ASSOC);

            $newsItem = $result->fetch();

            return $newsItem;
        }

    }

    /**
    * Returns an array of news items
    */
    public static function getNewsList() 
    {
        $db = DB::getConnection();
        $newsList = array();

        $result = $db->query('SELECT * FROM blog');

        $i = 0;
        while($row = $result->fetch()) {
            $newsList[$i]['id'] = $row['bl_id'];
            $newsList[$i]['date'] = $row['bl_date'];
            $newsList[$i]['title'] = $row['bl_title'];
            $newsList[$i]['short_text'] = $row['bl_short_text'];
            $newsList[$i]['text'] = $row['bl_text'];
            $i++;
        }

        return $newsList;
    
    }

}