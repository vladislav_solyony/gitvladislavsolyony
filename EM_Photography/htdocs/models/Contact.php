<?php


class Gallery
{

    /**
    * Returns an array of albums 
    */
    public static function getContactInfo() 
    {
        $db = DB::getConnection();
        $contactInfo = array();

        $result = $db->query("SELECT ma_name, ma_value FROM new_main WHERE ma_name LIKE '%contact%'");

        while($row = $result->fetch()) { 
            $contactInfo[$row['ma_name']] = $row['ma_value'];
        }

        return $contactInfo;
    
    }

}