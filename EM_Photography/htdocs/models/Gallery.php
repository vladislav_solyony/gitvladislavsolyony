<?php


class Gallery
{

    /** Returns single photo album with specified id
    * @rapam integer &id
    */

    public static function getPhotosItemByID($id)
    {
        $id = intval($id);
        
        $photosItem = array();

        if ($id) {
            $db = DB::getConnection();
            $result = $db->query('SELECT * FROM gallery_photos WHERE ga_id_album=' . $id);

            $result->setFetchMode(PDO::FETCH_ASSOC);

            $i = 0;
            while($row = $result->fetch()){
                $photosItem[$i]['id']    = $row['ga_id'];
                $photosItem[$i]['title'] = $row['ga_title'];
                $photosItem[$i]['link']  = $row['ga_link'];
                $photosItem[$i]['photo'] = $row['ga_photo'];
                $i++;    
            }

            return $photosItem;
        }

    }

    /**
    * Returns an array of albums 
    */
    public static function getAlbumsList() 
    {
        $db = DB::getConnection();
        $albumList = array();

        $result = $db->query('SELECT * FROM new_album');

        $i = 0;
        while($row = $result->fetch()) {
            $albumList[$i]['id']         = $row['al_id'];
            $albumList[$i]['name']       = $row['al_name'];
            $albumList[$i]['main_photo'] = $row['al_main_photo'];
            $albumList[$i]['title']      = $row['al_title'];
            $i++;
        }

        return $albumList;
    
    }

}