<?php


class About
{

    /**
    * Returns an array of albums 
    */
    public static function getAuthorInfo() 
    {
        $db = DB::getConnection();
        $authorInfo = array();

        $result = $db->query("SELECT ma_name, ma_value FROM new_main WHERE ma_name LIKE '%author%'");

        while($row = $result->fetch()) { 
            $authorInfo[$row['ma_name']] = $row['ma_value'];
        }

        return $authorInfo;
    
    }

}