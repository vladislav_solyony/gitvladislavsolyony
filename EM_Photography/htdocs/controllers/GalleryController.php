<?php

include_once ROOT. '/models/Gallery.php';

class GalleryController {

    public function actionIndex()
    {
        
        $albumList = array();
        $albumList = Gallery::getAlbumsList();

        require_once(ROOT . '/views/gallery/index.php');

        return true;
    }
    
    public function actionView($id)
    {
        if ($id) {
            $photosItem = Gallery::getPhotosItemByID($id);
            require_once(ROOT . '/views/gallery/view.php');
        }

        return true;

    }

}

