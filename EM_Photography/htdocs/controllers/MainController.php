<?php

include_once ROOT. '/models/Main.php';

class MainController {

    public function actionIndex()
    {
        
        $sliderPhotos = array();
        $sliderPhotos = Main::getSliderPhotos();

        require_once(ROOT . '/views/main/index.php');

        return true;
    }

}

