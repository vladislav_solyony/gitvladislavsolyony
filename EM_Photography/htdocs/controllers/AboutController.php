<?php

include_once ROOT. '/models/About.php';

class AboutController {

    public function actionIndex()
    {
        
        $authorInfo = array();
        $authorInfo = About::getAuthorInfo();

        require_once(ROOT . '/views/about/index.php');

        return true;
    }

}

