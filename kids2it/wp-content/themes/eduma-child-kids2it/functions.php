<?php

/**
 * enqueue registration-form.js and ajax_url
 */
function registration_form_enqueue() {
    wp_enqueue_script( 'registration-form-js', get_stylesheet_directory_uri() . '/registration-form.js', array( 'jquery' ) );
    wp_localize_script( 'registration-form-js', 'reg_form', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'registration_form_enqueue' );


add_action( 'wp_ajax_registration_form', 'eduma_registration_mail_send' );
add_action( 'wp_ajax_nopriv_registration_form', 'eduma_registration_mail_send' );


/**
 * send registration message(user personal data & course name) to admin's e-mail
 */
function eduma_registration_mail_send() {
    if ( isset( $_POST['eduma_submit_reg_form'] ) && wp_verify_nonce( $_POST['eduma_submit_reg_form'], 'eduma_submit_reg_form' ) ) {
        $course_name = $_POST['course_name'];
        $name        = $_POST['name'];
        $last_name   = $_POST['last_name'];
        $birth_date  = $_POST['birth_date'];
        $location    = $_POST['location'];
        $email       = $_POST['e-mail'];

        $message = $name . $last_name . $birth_date . $location . $email;

        if ( wp_mail( get_option( 'admin_email' ), $course_name, $message ) ) {
            $response = array(
                'status'  => true,
                'message' => __( 'Message successfully sent' ),
            );
        } else {
            $response = array(
                'status'  => false,
                'message' => __( 'Message didn\'t send' ),
            );
        }
        wp_die( json_encode( $response ) );
    }
}


/**
 * add shortcode with course registration form
 */
function registration_mail_shortcode() {
    /** @var LP_Course $course */
    $course = LP()->global['course']; ?>

    <form name="registration_form" id="registration_form">
        <label for="name"><?php _e( 'Name' ); ?>:</label>
            <input type="text" value="" name="name" id="name" required><br/>

            <label for="last_name"><?php _e( 'Last name' ); ?>:</label>
            <input type="text" value="" name="last_name" id="last_name" required><br/>

            <label for="birth_date"><?php _e( 'Date of birth' ); ?>:</label>
            <input type="date" value="" name="birth_date" id="birth_date" required><br/>

            <label for="location"><?php _e( 'Location' ); ?>:</label>
            <input type="text" value="" name="location" id="location" required><br/>

            <label for="e-mail"><?php _e( 'E-mail' ); ?>:</label>
            <input type="email" name="e-mail" id="e-mail" required><br/>
            <input type="hidden" name="action" value="registration_form" required><br/>

            <input type="hidden" name="course_name" id="course_name" value="<?php echo $course->get_title( $course->ID ); ?>">
            <input type="submit" value="<?php _e( 'Register' ); ?>" id="reg_sub">
            <?php wp_nonce_field( 'eduma_submit_reg_form', 'eduma_submit_reg_form' ); ?>
    </form>
    <div id="eduma-response"></div>

<?php
}
add_shortcode( 'registration_mail_send', 'registration_mail_shortcode' );
