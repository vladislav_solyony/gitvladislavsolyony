(function($){

    'use strict';

    $(function() {
        $('#registration_form').on('submit', function() {

            $( this ).find( 'input' ).prop( 'readonly', true );
            $( this ).find( 'input[type=submit]' ).prop( 'disabled', true );

            $.ajax({
                type:     "post",
                dataType: "json",
                url:      reg_form.ajax_url,
                data:     $( this ).serialize(),
                success: function( response ) {

                    response = response || {};

                    if ( response.status ) {
                        $( '#eduma-response' ).html( response.message );
                    }

                    $( this ).find( 'input' ).prop( 'readonly', false ).prop( 'disabled', true );
                },
                error: function() {
                    $( '#eduma-response' ).html( "Failed to send message" );
                }
            });
            return false;
        });
    });
})(jQuery);
