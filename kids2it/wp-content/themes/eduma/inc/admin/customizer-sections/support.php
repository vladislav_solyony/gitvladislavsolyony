<?php
/*
 * Creating a logo Options
 */

$support = $titan->createThemeCustomizerSection( array(
	'id'       => 'support-documentation',
	'position' => 1,
	'name'     => esc_html__( 'Support & Documentation', 'eduma' ),
	'desc' => '<div class="support-documentation">
					<a href="https://themeforest.net/item/education-wordpress-theme-education-wp/14058034">Theme Details</a>
					<a href="http://docs.thimpress.com/eduma">Theme Documentation</a>
					<a href="https://wordpress.org/plugins/learnpress/">LearnPress Community</a>
					<div class="description">
						<p>Want to ask <b>support question</b> or <b>report bug</b>? Please use our <a href="#" title="(Support\'s response time can be up to 1 business day. Our time zone is GMT+7)">support forum.</a></p>
						<p>Already submitted a support topic for <b>more than 3 days</b> but it hasn\'t been answered or the answer is not up to your expectation yet?</p>
						<p><b>Your ticket may have got lost somewhere!</b> Please send <a href="mailto:tungnd@foobla.com">Tung</a> an email and he will assign your issue directly to a <b>developer team member</b>.</p>
						<p>If you like this theme, I\'d appreciate any of the following:</p>
					</div>
					<div class="ratings-social">
						<a href="https://themeforest.net/downloads">Rate this Theme</a>
						<a href="https://www.facebook.com/ThimPress/">Like on Facebook</a>
						<a href="https://twitter.com/thimpress">Follow on Twitter</a>
						<a href="https://www.youtube.com/c/ThimPressDesign">Subscribe Youtube</a>
					</div>
			   </div>'
) );
//
$support->createOption(
	array(
		'name' => 'Test options',
		'id' => 'my_text_option',
		'type' => 'text',
		'desc' => 'This is our test option'
	)
);
