<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'kids2it_DB');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '123');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!g!i6v v~X{s&GK9$]$1#`O_&2p[Ogmd,0xLQg&q3,O@KD*c|=j*!wG7sNSp-^_)');
define('SECURE_AUTH_KEY',  '^bGr6WASkslh`*m(xs6GvcRu]ApsyW+ cHMv`u+pXDl&@v0(Yh)}Uz`Y$>4>hise');
define('LOGGED_IN_KEY',    'h;%sP,/ H@|Wm?i}YVWb4o/j>KcDa.k8Bg{)fW!?);[/4$oEle<C0 n0)<vX+?Tf');
define('NONCE_KEY',        ';?MdD>a-1NSzpl5_jh+WAHAY_<N]vGVg#Q,m1& AoLHOrH ]<4)%|gu&~!+Iteor');
define('AUTH_SALT',        'mQ%iw4+eCzWz0DD/oi4/~W5X++pOXH81wQZ}SG.?$p@L%eg,l7GsKwfe#eR[ig$(');
define('SECURE_AUTH_SALT', ';+0q)vWXtu3H$6:Jv=.S1{.?O@azp;.BHZ)c]T^+zC^%X;S[6UY=f98Q/& GeX0=');
define('LOGGED_IN_SALT',   '5a<pg*95~j SW}Pb]E7:L45J-IxEen`1.F:!`@p9=BO1mfg5:4sYzI:k6z4r2]Ih');
define('NONCE_SALT',       'G(I}2b!UtqAt[hJ{D|{ ~1h6f6eEgXX+15TU*jWw#`SY+RN+LKnkXAi7Sa4R,c:[');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'kids_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
if(is_admin()) {
    add_filter('filesystem_method', create_function('$a', 'return "direct";' ));
    define( 'FS_CHMOD_DIR', 0751 );
}

