<?php 
/*
Plugin Name: WooCommerce custom checkoutfield Plugin 
Plugin URI:  -
Description: Plugin to add custom checkout field in WooCommerce.
Version:     1.0
Author:      Vladislav Solyony
Author URI:  -
Text Domain: wporg
Domain Path: -
License:     GPL2
 
WooCommerce custom checkoutfield Plugin  is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
WooCommerce custom checkoutfield Plugin  is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {License URI}.
*/

require_once (plugin_dir_path( __FILE__) . 'woocommerce-custom-checkoutfield-plugin-add-new.php' );

function woocommerce_custom_checkoutfield_install()
{
    // trigger our function that registers the custom post type
    events_register_post_type();
 
    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'woocommerce_custom_checkoutfield_install' ); //Activation Hook

//DEACTIVATION
function woocommerce_custom_checkoutfield_deactivation()
{
    // our post type will be automatically removed, so no need to unregister it
 
    // clear the permalinks to remove our post type's rules
    flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'woocommerce_custom_checkoutfield_deactivation' ); //Deactivation Hook
