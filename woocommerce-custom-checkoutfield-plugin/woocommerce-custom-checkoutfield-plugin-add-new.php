<?php

/**
 * Add the field to the checkout
 */
add_action( 'woocommerce_after_order_notes', 'my_custom_checkout_field' );

function my_custom_checkout_field( $checkout ) {

    echo '<div id="my_custom_checkout_field"><h2>' . __('Tell us about yourself') . '</h2>';

    woocommerce_form_field( 'personal_info_field', array(
        'type'          => 'text',
        'class'         => array( 'my-field-class form-row-wide' ),
        'label'         => __( 'Fill in this field' ),
        'placeholder'   => __( 'Enter your personal info' ),
        ), $checkout->get_value( 'personal_info_field' ) );

    echo '</div>';

}

/**
 * Process the checkout
 */
add_action( 'woocommerce_checkout_process', 'my_custom_checkout_field_process' );

function my_custom_checkout_field_process() {
    // Check if set, if its not set add an error.
    if ( ! $_POST['personal_info_field'] )
        wc_add_notice( __( 'Please enter something into this new shiny field.' ), 'error' );
}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['personal_info_field'] ) ) {
        update_post_meta( $order_id, 'Personal information', sanitize_text_field( $_POST['personal_info_field'] ) );
    }
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__( 'Personal information' ).':</strong> ' . get_post_meta( $order->id, 'Personal information', true ) . '</p>';
}


/**
 * Sending custom checkout field value to e-mail
 */
add_filter( 'woocommerce_email_order_meta_keys', 'my_custom_order_meta_keys' );

function my_custom_order_meta_keys( $keys ) {
     $keys[] = 'Personal information'; // This will look for a custom field called 'Tracking Code' and add it to emails
     return $keys;
}
